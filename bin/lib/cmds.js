// This file is about Pscaff Commands (Custom commands defined by the user) to add lines of dynamic code to template files.
// TODO: Add info/url to docs
/*
PScaff (Project Scaffolding)
Copyright (C) 2018 - Energy1011[at]gmail(dot)com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';


var cmds = {};
// internals
var i              = {};
    i.fs           = require('fs');
    i.path         = require('path');
    i.projectConf  = {};
    i.userCmdsFile = {};
    i.pstags       = {};
    // active and deactivate this module if cmds file exists
    i.isModuleEnable = true;

cmds.checkPSCommandExist = function(pscName){
  return (typeof i.userCmdsFile[pscName] != 'undefined');
}

cmds.execPSCommand = function (pscmd){
  console.log('[i] Executing PSCommand found: '+pscmd.pscName);
  return i.userCmdsFile[pscmd.pscName](pscmd);
}

cmds.outputPSCommand = function(d, ln, pscmd){
  var nS = d[ln].split(" ").length-1; // total spaces of //PSC tag
  var nT = d[ln].split("\t").length-1; // total tabs of //PSC tag
  var space = ' ';
  //TODO: add tabs in lines
  //add line spaces
  for(var s=1; s< nS; s++) space += ' ';
  // Count lines in pscommand zone
  pscmd.czIsEmpty = ( pscmd.pscb >= pscmd.psce - 2 ) ? true : false;
  pscmd.czCount = (pscmd.psce - pscmd.pscb) - 2;
  var o = cmds.execPSCommand(pscmd);
  // iter over output command array
  for(var e in o){
    // TODO: set lang for replace Tags
    o[e] = i.pstags.replaceTags(o[e],'en');
    d.splice(ln,0,space+o[e]);
    ln++;
  }
  return d;
}

cmds.getPSCommandName = function (line){
  // get command name getting first the first '-'
  return line.substring(line.indexOf("-")+1);
}

cmds.addPSCommand = function (pscmds, tag, line, lineNum, file){
  var pscName = cmds.getPSCommandName(line);
  // Check if command is exists and defined
  if(!cmds.checkPSCommandExist(pscName)){
    console.log('[!] PSCommand: \''+pscName+'\' is not defined in <yourprojectfolder>/pscaff/config/{proyect}/{layerName}/cmds.js file, create it if you need it or remove this call from file: '+file+':'+lineNum);
    return pscmds;
  }
  if(typeof pscmds[pscName] == 'undefined') pscmds[pscName] = {};
  // set command and tag line position
  pscmds[pscName]['pscName'] = pscName;
  pscmds[pscName][tag] = lineNum;
  return pscmds;
}

cmds.findPSCommand = function(file, pstags, projectConf, action = 'scaff'){
  // set internals
  i.pstags = pstags;
  i.projectConf = projectConf;
  // Check if cmds file exists and this module is Enable
  if (i.isModuleEnable){ // TODO: implement it in a better way
    if(!i.fs.existsSync(projectConf.rootPath+'config'+i.path.sep+'cmds'+i.path.sep+i.projectConf.name+i.path.sep+i.projectConf.layerName+i.path.sep+'cmds.js')){
      i.isModuleEnable = false;
      console.log('[i] Pscommand config file not found then PScommand module is disable');
      console.log("[i] If you want to able PScommand module you need to create the following file: " +projectConf.rootPath+'config'+i.path.sep+'cmds'+i.path.sep+i.projectConf.name+i.path.sep+i.projectConf.layerName+i.path.sep+'cmds.js');
      // there is no config/.../cmds.js file... bye, bye
      return
    }
    // Set userCmdsFile defined as config/.../cmds.js
    i.userCmdsFile = require(projectConf.rootPath+'config'+i.path.sep+'cmds'+i.path.sep+i.projectConf.name+i.path.sep+i.projectConf.layerName+i.path.sep+'cmds.js');
  }else{
    console.log('[i] Pscommand module not enable');
    return
  }

  //if file path doesnt exists skip
  if (!i.fs.existsSync(file.path)){
    return console.log('[i] File ' + file.path + ' does not exist.');
  }

  var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(file.path)
  });
  var pscmds = {};
  var c = 1;

  function iterPSCommands(file, action){
    return (e)=>{
      // get file data array, split by return
      //TODO validate new line type \r\n or \n
      var d = i.fs.readFileSync(file.path).toString().split("\n");
      var dInitSize = d.length;
      if(dInitSize <= 1) return console.log('[Error] Lines in file <= 1, current size is: '+dInitSize+' for '+ file.path);
      for (c in pscmds){
        // iter over the pscommand zone
        if(action == 'scaff'){
          for(var ln = pscmds[c].pscb; ln <= pscmds[c].psce; ln++){
            if(ln == pscmds[c].psc-1){
              ln = ln + (d.length - dInitSize);
              d = cmds.outputPSCommand(d, ln, pscmds[c]);
            }
          }
        }else{
          d.splice(pscmds[c].pscb -1 ,0 ,'<<<<<<< PScaff IMPORTANT BEGIN - Remove lines about ('+projectConf.resourceName+') resource in this marked PScommand zone.');
          d.splice(pscmds[c].psce +1,0 ,'>>>>>>> PScaff IMPORTANT END');
        }
        var writer = i.fs.createWriteStream(file.path);
        writer.on('error', function(err) { console.log('Error writeStream');});
        d.forEach(function(l) { writer.write(l + '\n'); });
        writer.end();
      }
    }
  }

  lineReader.on('line',function(line){
    // determinate if in this file we have PSC tags
    if(line.match(/\/\/PSCB-/) != null) pscmds = cmds.addPSCommand(pscmds, 'pscb', line, c, file.path);
    if(line.match(/\/\/PSC-/) != null) pscmds = cmds.addPSCommand(pscmds, 'psc', line, c, file.path);
    if(line.match(/\/\/PSCE-/) != null) pscmds = cmds.addPSCommand(pscmds, 'psce', line, c, file.path);
    c++;
  });
  lineReader.on('close',iterPSCommands(file, action));
}
// Export main object tpls
module.exports = cmds;
