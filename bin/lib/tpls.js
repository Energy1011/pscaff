// This file is about templates inside pscaff/bin/config/templates, read and write them
// TODO: Add info/url to docs
/*
PScaff (Project Scaffolding)
Copyright (C) 2018 - Energy1011[at]gmail(dot)com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';
// Declare main export obj
var tpls = {};

// Declare internals
var i      = {};
    i.fs   = require('fs'),
    i.path = require('path'),
    i.pscmds = require('./cmds');

tpls.mkdirsInPath = function (path){
  // Remove filename from path
  path = path.substring(0, path.lastIndexOf(i.path.sep));
  var dirs = path.split(i.path.sep);
  var last = '';
  for(var d in dirs){
    dirs[d] = last+dirs[d]+i.path.sep;
    // Check if path destination exists
    if (!i.fs.existsSync(dirs[d])){
        console.log('[i] Creating path:'+dirs[d]);
        i.fs.mkdirSync(dirs[d]);
    }
    last = dirs[d];
  }
}

tpls.createTplFile = function (file, pstags, projectConf){
  var joinedFilePath = i.path.join(file.tplName+'.tpl');
  if(projectConf.alloverwrite != '') file.overwrite = projectConf.alloverwrite;
  if((typeof file.overwrite != 'undefined' && file.overwrite == false && i.fs.existsSync(file.path))){
    console.log('[Ok] Tpl found with overwrite protection (just exec PSCommads): '+file.path);
    if(projectConf.alloverwrite != '') console.log('[Global All Overwrite] -O option set to: '+projectConf.alloverwrite);
    // We dont need to create/scaff this template, just find PSCommands
    // Check for PScommands
    i.pscmds.findPSCommand(file, pstags, projectConf);
  }else{
    // Read pscaff defined template and write to disk
    // Check if template exists
    if (i.fs.existsSync(joinedFilePath)) {
      i.fs.readFile(joinedFilePath, {encoding: 'utf-8'}, function(err,data){
        if (!err) {
          // Replace pscaff tags
          data = pstags.replaceTags(data,file.tplLang);
          // Mkdirs for paths
          tpls.mkdirsInPath(file.path);
          // Write the file in its destination path
          i.fs.writeFile(file.path, data, function(err) {
            if(err) {
              console.log('[Error] Writing template file: ' + err);
            } else{
              console.log('[Ok] Tpl processed (writed/created): ' + file.path);
              // Check for PScommands
              i.pscmds.findPSCommand(file, pstags, projectConf);
            }
          });
        } else {
          console.log('[Error] Error reading template file: ' + joinedFilePath);
          console.log(err);
        }
      });
    }else{
      console.log('[i] Tpl doesn\'t exist in:' + joinedFilePath + ' please create this file inside pscaff config if it\'s needed to scaff in your project.');
    }
  }
}


tpls.unscaffTplFile = function (file, pstags, projectConf){
  var joinedFilePath = i.path.join(file.tplName+'.tpl');
  if(projectConf.alloverwrite != '') file.overwrite = projectConf.alloverwrite;
  if((typeof file.overwrite != 'undefined' && file.overwrite == false)){
    console.log('[Ok] Tpl found with overwrite protection (PSCommads): '+file.path);
    if(projectConf.alloverwrite != '') console.log('[Global All Overwrite] -O option set to: '+projectConf.alloverwrite);
    // Check for PScommands
    i.pscmds.findPSCommand(file, pstags, projectConf, 'unscaff');
  }else{
    // Check if template exists
    if (i.fs.existsSync(file.path)) {
      // Delete file
      // TODO remove migration files
      i.fs.unlink(file.path, ()=>{
        console.log('[i] Unscaff file : '+file.path);
      });
    }
  }
}

// Export main object tpls
module.exports = tpls;
