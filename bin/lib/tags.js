// This file is about tags, replacing tags like <r_plural_slug_c>
// TODO: Add info/url to docs
/*
PScaff (Project Scaffolding)
Copyright (C) 2018 - Energy1011[at]gmail(dot)com
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

var S    = require('string'),
    DATE = require('date-and-time');

// Declare main export obj
var tags = [];

// Set a default lang init
tags['xLang'] = {
  r_original_c               : '',
  r_plural_lower_c           : '',
  r_singular_lower_c         : '',

  r_singular_underscore_c    : '',
  r_plural_underscore_c      : '',

  r_singular_studly_c        : '',
  r_plural_studly_c          : '',

  r_singular_prefix_studly_c : '',
  r_plural_prefix_studly_c   : '',

  r_singular_slug_c          : '',
  r_plural_slug_c            : '',

  r_singular_camel_c         : '',
  r_plural_camel_c           : '',

  r_singular_uc_first_c      : '',
  r_plural_uc_first_c        : ''
};

// Args from pscaff call to use as params
tags.prms = [];
// Set params if exist
tags.setParams = function (program){
  if( program.params.length > 0){
    for(let p in program.params){
        tags.prms.push(program.params[p]);
    }
    console.log('[i] Params: ', tags.prms);
  }
}

// Resolve the param tag
tags.resolvePrm= function(prm){
    if(typeof tags.prms[prm] != 'undefined'){
      return tags.prms[prm];
    }
  return prm;
}

tags.replacePrmTags = function(str){
  let prmMatch = str.match(/<prm_.+?>/g);
  if(prmMatch){
    // console.log('[i] PSparamTag found.');
    for (let m in prmMatch){
      let regex = new RegExp(prmMatch[m], "g");
      str = str.replace(regex, tags.resolvePrm(prmMatch[m].substring(5).slice(0, -1)));
    }
  }
  return str;
}

tags.toPrefixStudlyCase = function(str){
  // Example where CAT_ is a prefix: CAT_MyStudlyCASE
  // result: CatMyStudlyCASE
  var pos = -1;

  pos = str.indexOf("_");
  if(pos == -1) return S(str).humanize().capitalize().camelize().s;
  var part1, part2 = ''
  part1 = S(str.substring(0, pos)).capitalize().s;
  part2 = str.substring(pos, str.length).replace(/([A-Z])/g, '_$1');
  part2 = S(part2).humanize().capitalize().camelize().s;
  return part1 + part2;
}

// Resource converted cases
tags.setResourceCases = function (program) {
    let langs = program.resourceConf.langs;
    for(var i in langs){
      tags[langs[i].ISOCode]                            = {};

      // Resource name original case
      tags[langs[i].ISOCode].r_original_c               = program.resourceConf.name;

      // lower
      tags[langs[i].ISOCode].r_singular_lower_c         = S(langs[i].singular.toLowerCase()).s;
      tags[langs[i].ISOCode].r_plural_lower_c           = S(langs[i].plural.toLowerCase()).s;

      // underscore
      tags[langs[i].ISOCode].r_singular_underscore_c    = S(langs[i].singular).underscore().s;
      tags[langs[i].ISOCode].r_plural_underscore_c      = S(langs[i].plural).underscore().s;

      // studly
      tags[langs[i].ISOCode].r_singular_studly_c        = S(langs[i].singular).humanize().capitalize().camelize().s;
      tags[langs[i].ISOCode].r_plural_studly_c          = S(langs[i].plural).humanize().capitalize().camelize().s;

      // special prefix studly
      tags[langs[i].ISOCode].r_singular_prefix_studly_c = tags.toPrefixStudlyCase(langs[i].singular);
      tags[langs[i].ISOCode].r_plural_prefix_studly_c   = tags.toPrefixStudlyCase(langs[i].plural);

      // slug
      tags[langs[i].ISOCode].r_singular_slug_c          = S(langs[i].singular.toLowerCase()).slugify().s;
      tags[langs[i].ISOCode].r_plural_slug_c            = S(langs[i].plural.toLowerCase()).slugify().s;

      tags[langs[i].ISOCode].r_singular_camel_c         = S(langs[i].singular).camelize().s;
      tags[langs[i].ISOCode].r_plural_camel_c           = S(langs[i].plural).camelize().s;

      // UC first
      tags[langs[i].ISOCode].r_singular_uc_first_c      = S(langs[i].singular).capitalize().s;
      tags[langs[i].ISOCode].r_plural_uc_first_c        = S(langs[i].plural).capitalize().s;
    }
}

tags.resolveTs = function(tsFormat){
  let s = '';
  let now = new Date();
  if(tsFormat == 'unix_seg'){
    var unix = Math.round(+new Date()/1000);
    s = unix;
  }else{
    s =  DATE.format(now, tsFormat);
  }
  return s;
}

tags.replaceTSTags = function(str){
  let tsMatch = str.match(/<ts_.+?>/g);
  if(tsMatch){
    for (let m in tsMatch){
      let regex = new RegExp(tsMatch[m], "g");
      str = str.replace(regex, tags.resolveTs(tsMatch[m].substring(4).slice(0, -1)));
    }
  }
  return str;
}

tags.replaceTags = function (str, lang='xLang'){
 // Replace custom Expressions
  // using /g for all occurrences
  if (tags[lang] === undefined)
    return str;
  str = str.replace(/<r_original_c>/g,tags[lang].r_original_c);

  str = str.replace(/<r_singular_lower_c>/g,tags[lang].r_singular_lower_c);
  str = str.replace(/<r_plural_lower_c>/g,tags[lang].r_plural_lower_c);

  str = str.replace(/<r_singular_studly_c>/g,tags[lang].r_singular_studly_c);
  str = str.replace(/<r_plural_studly_c>/g,tags[lang].r_plural_studly_c);

  str = str.replace(/<r_singular_prefix_studly_c>/g,tags[lang].r_singular_prefix_studly_c);
  str = str.replace(/<r_plural_prefix_studly_c>/g,tags[lang].r_plural_prefix_studly_c);

  str = str.replace(/<r_singular_underscore_c>/g,tags[lang].r_singular_underscore_c);
  str = str.replace(/<r_plural_underscore_c>/g,tags[lang].r_plural_underscore_c);

  str = str.replace(/<r_singular_slug_c>/g,tags[lang].r_singular_slug_c);
  str = str.replace(/<r_plural_slug_c>/g,tags[lang].r_plural_slug_c);

  str = str.replace(/<r_singular_camel_c>/g,tags[lang].r_singular_camel_c);
  str = str.replace(/<r_plural_camel_c>/g,tags[lang].r_plural_camel_c);

  str = str.replace(/<r_singular_uc_first_c>/g,tags[lang].r_singular_uc_first_c);
  str = str.replace(/<r_plural_uc_first_c>/g,tags[lang].r_plural_uc_first_c);

  // Replace tsTags time stamp (ts) tags
  str = tags.replaceTSTags(str);

  // Replace <prm_*> params
  if(tags.prms.length >= 0){
    str = tags.replacePrmTags(str);
  }
  return str;
}

// Export main object tpls
module.exports = tags;
