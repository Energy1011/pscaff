# Project Scaffolding [![Build Status](https://travis-ci.org/Energy1011/pscaff.svg?branch=master)](https://travis-ci.org/Energy1011/pscaff)
![PScaff](https://gitlab.com/Energy1011/pscaff/raw/master/assets/pscaff-logo.png)

## __Usage:__ PScaff command [options]
```
$ pscaff -h
Options:
  -V, --version                                 output the version number
  -r, --resource [resourceName]                 Set resource name, Example: apple
  -p, --resource-plural [resourceNameInPlural]  Set resource name, Example: apples
  -e, --editor [editor]                         Use your favorite editor default:[sublime_text] (default: "sublime_text")
  -a, --action [actionName]                     Resource action [open|scaff] (default: "open")
  -f, --projectfile [projectfile]               JSON project conf file (default: "default")
  -s, --set [set]                               Wich set of template files (default: "files")
  -t, --templates [templates]                   Specify templates by tplName (divided by commas, example: -t 2,4,6) to use when call pscaff
  --prm, --params [params]                      User args used as pscaff params tags (divided by commas)
  -R, --resourcefile [resourcefile]             JSON resource file (default: "default")
  -O, --alloverwrite [alloverwrite]             Set overwrite value [true|false] for (All) templates
  -h, --help                                    output usage information

```
## __The main goal of PScaff__
The main goal is to run commands like this
```bash
$ pscaff -a scaff -R penguin
```
To scaff/unscaff/open in editor the PSResource, to achive these we need first to define 3 config files:
- __PSresource__: In this file we are going to define the resource (object) in singular, plural and languages to use with it.
- __PSproject__: In this file we define paths/langs for our PStpls.
- __PStpl__: In this template where we write code/text and PStags (no matter what programming language, no matter what type of conent) it is a template that Pscaff is going to re-use each time we call pscaff with a certain PSresource name with -R option.

## __How to define a PSresource__ (json config file)
[Example of this here](https://gitlab.com/Energy1011/pscaff/blob/master/bin/config/resources/user.json)

To define for example the 'penguin' resource we need to create a file as __/config/resources/penguin.json__ and its contents
```javascript
{
  "name":"penguin",
  "langs":[
    { "ISOCode":"en", "singular":"penguin", "plural":"penguins"},
    { "ISOCode":"es", "singular":"pingüino", "plural":"pingüinos"}
  ]
}
```
## __Defining a PSproject__ (json config file)
[Example of this here](https://gitlab.com/Energy1011/pscaff/blob/master/bin/config/projects/default.json)

In this file we define our templates to use with pscaff.
For example if we want to open and/or scaff template files for a laravel php project we can define of project config file like this:
```json
TIP: This is the default set of files array property to define the default (main) set of files.
      "files": [
        {"tplName": "1", "path":"resources/lang/es/crudvel/<r_plural_slug_c>.php"},
        {"tplName": "2", "path":"app/Models/<r_singular_studly_c>.php"},
        {"tplName": "3", "path":"app/Http/Requests/<r_singular_studly_c>Request.php"},
        {"tplName": "4", "path":"app/Http/Controllers/Api/<r_singular_studly_c>Controller.php"},
        {"tplName": "5", "path":"database/seeds/<r_singular_studly_c>TableSeeder.php"}
      ]
```
All <r_*> listed in the next section will be replace with the resource name given, with the selected convertion case.

## __PStags for resource convertion cases__
A PSTag is a tag where pscaff resplaces/converts its place in resource cases.
Example: Pscaff will replace/convert this PStag __<r_plural_studly_c> to 'Penguins'__ when we call pscaff like this:
```bash
$ pscaff -a scaff -R penguin
```

## __Available resource PStags__
```html
<r_singular_lower_c> = singular resource name in lower case (spaces converted to underscore)
<r_plural_lower_c> = plural resource name in lower case (spaces converted to underscore)

<r_singular_underscore_c> = singular resource_name in under_score
<r_plural_underscore_c> = plural resource_name in under_score

<r_singular_studly_c> = singular ResourceName in StudlyCase (the same as camel case but it starts with CapitalCase)
<r_plural_studly_c> = plural ResourceName in StudlyCase( the same as camel case but it starts with CapitalCase)

<r_singular_slug_c> = singular resource-name in slug-case
<r_plural_slug_c> = plural resource-name in slug-case

<r_singular_camel_c> = singular resource name in camelCase (camelCase doesn't start with capital case)
<r_plural_camel_c> = plural resource name in camelCase (camelCase doesn't start with capital case)

<r_singular_uc_first_c> = singular UCF Resource name (preserve the epaces)
<r_plural_uc_first_c> = plural UCF Resource name (preserve the spaces)

```

## __How to define a PStpl (template file)__
[Example of this here](https://gitlab.com/Energy1011/pscaff/blob/master/bin/config/templates/default/backend/6.tpl)

Pscaff will replace all PStags and PScommands inside it when we call it for a certain resource, no matter what programming language are you using in your project.

Imagine that we have a php project and we want to scaff this file for earch resource (object), then we define our PStpl like this in [config/templates/default/backend](https://gitlab.com/Energy1011/pscaff/tree/master/bin/config/templates/default/backend)
```php
<?php
return [
    'fields' => [
		'active'     => 'Active',
		'created_at' => 'Created at'
		'created_by' => 'Created by',
		'id'         => 'Id',
		'updated_at' => 'Updated at',
		'updated_by' => 'Updated by',
    ],
    'foreign_fields'=>[
		'id'         => '<r_singular_uc_first_c> id',
		'active'     => '<r_singular_uc_first_c> active',
		'created_at' => '<r_singular_uc_first_c> created at',
		'created_by' => '<r_singular_uc_first_c> created by',
		'created_by' => '<r_singular_uc_first_c> updated by',
		'updated_at' => '<r_singular_uc_first_c> updated at',
    ],
	'row_label'  => '<r_singular_uc_first_c>',
	'rows_label' => '<r_plural_uc_first_c>',
	'actions'    => [
    ],
];
```

## __How to open project 'resource' files in your favorite editor pretty fast__
To open all files of a defined resource, for example 'penguin':
```bash
$ pscaff -r penguin -p penguins -a open
# or specifying the editor
$ pscaff -r penguin -p penguins -a open -e vim
```
The last command will open all files defined in PSProject config file.

## __How to define own set of files inside the PSproject json config file__
We can use set of files to custom and group particular templates with some variations from the default set of files named 'files' as property.
Imagine taht we need a set of files named __SextX__, then we need to define set of files property like __"filesSetx"__ like following:
```json
TIP: Pscaff recognize this property with pre-concatenated 'files' to your set name.
      "filesSetX": [
        {"tplName": "1", "path":"resources/lang/es/crudvel/<r_plural_slug_c>.php"},
        {"tplName": "2", "path":"app/Models/<r_singular_studly_c>.php"},
        {"tplName": "3", "path":"app/Http/Requests/<r_singular_studly_c>Request.php"},
        {"tplName": "4", "path":"app/Http/Controllers/Api/<r_singular_studly_c>Controller.php"},
        {"tplName": "5", "path":"database/seeds/<r_singular_studly_c>TableSeeder.php"}
      ]
```
 As defined above, we can call __pscaff__ command with __-s__ option specifying the set of files _'SetX'_ to use
```bash
$ pscaff -a scaff -R penguin -s SetX
```

## __Special PStags__
Sometimes we need to name a file or add a code with a certain time format in a timestamp, for that we can use the <ts_*> PStag for time
In our project config json we can have a defined a tpl path like this:
```json
        {"tplName": "8", "tplLang":"en", "fileNameLang":"en", "path":"database/migrations/<ts_YYYY_MM_DD>_<ts_unix_seg>_create_<r_plural_underscore_c>_table.php"},
```

## __How to build a <ts_*> tag__
Pscaff is flexible with ts tags, and that flexibility comes requiring 'date-and-time' library, and pscaff do this in its __lib/tags.js__ module. Then we can use <ts_*> as 'date-and-time' library allows to define timestamps formats, only we replace the * inside <ts_*> tag with the desired format.

Examples:
```html
// year_month_day format
<ts_YYYY_MM_DD>
// we can change the separators:
<ts_YYYY-MM-DD>
// or not use separators
<ts_YYYY MM DD>
// change year, month,day position
<ts_MM DD YY>
.... Many as date-and-time library allow us


// Aditionally we have a unix seg tag
<ts_unix_seg>
```
For more info check ['date-and-time'](https://www.npmjs.com/package/date-and-time) library time formats [https://www.npmjs.com/package/date-and-time](https://www.npmjs.com/package/date-and-time).

## __Opening files with sublime_text, needed for Windows users__
For windows users, run this command as admin to allow call sublime_text from cmd globally from cmd
```cmd
mklink /h c:\windows\system32\sublime_text.exe "c:\Program Files\Sublime Text 3\sublime_text.exe"
```
